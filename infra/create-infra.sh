#!/usr/bin/env bash

# Usage: ./create-infra.sh <stage> <gitlab username> <gitlab token>
set -e
set -u
set -o pipefail

if [ $# != 3 ]
  then
    echo "Invalid number of arguments supplied. Usage: ./create-infra.sh <stage> <gitlab username> <gitlab token>"
    exit 1
fi

stage=$1
gitlab_username=$2
gitlab_token=$3

cp -a common/. ${stage}

cd ${stage}

terraform init \
 -backend-config="backend-config.conf" \
 -backend-config="username=${gitlab_username}" \
 -backend-config="password=${gitlab_token}"


terraform apply
