terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "2.14.0"
    }
  }
  backend "http" {}
}

terraform {

}

provider "digitalocean" {
  token = var.do_token
}

resource "digitalocean_kubernetes_cluster" "this" {
  name   = var.environment
  region = var.do_region
  version = var.k8s_version

  node_pool {
    name       = "worker-pool"
    size       = var.node_size
    node_count = var.node_count
  }
}

resource "digitalocean_domain" "this" {
  name = var.domain
}

# Add an A record to the domain for www.example.com.
resource "digitalocean_record" "this" {
  domain = digitalocean_domain.this.name
  type   = "A"
  name   = var.hostname
  value  = var.ipv4_address
}