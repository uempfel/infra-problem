variable "do_token" {
    type = string
    description = "API Token for DigitalOcean account (https://docs.digitalocean.com/reference/api/create-personal-access-token/)."
}

variable "node_count" {
    type = string
    description = "Number of cluster nodes to provision"
}


variable "node_size" {
    type = string
    description = "Node size of the cluster's nodes. Run `doctl compute size list` to get available sizes."
}

variable "k8s_version" {
    type = string
    description = "Kubernetes version to install. Run `doctl kubernetes options versions` to get the latest version slugs."
}

variable "do_region" {
    type = string
    description = "DigitalOcean region slug to deploy the k8s cluster to. Defaults to `fra1`. For other regions refer to https://docs.digitalocean.com/products/platform/availability-matrix/"
    default = "fra1"
}

variable "environment" {
    type = string
    description = "The environment to provision the cluster for (eg. dev, qa, prod). Will also be the cluster's name."
}

variable "domain" {
    type = string
    description = "Domain or subdomain for the DNS record. E.g. `staging.example.com`."
}

variable "hostname" {
    type = string
    description = "Hostname for the DNS record."
    default = "app"
}

variable "ipv4_address" {
    type = string
    description = "IPv4 address of the front-end's LoadBalancer service."
}
