node_size = "s-1vcpu-2gb"
node_count = "1"
k8s_version = "1.21.3-do.0"
do_region = "fra1"
environment = "dev"
domain = "dev.uempfel.de"
hostname = "infra-problem"
# Dummy value for the initial infrastructure deployment.
# Needs to be updated after the front-end's real IP is known
ipv4_address = "157.230.76.23"
