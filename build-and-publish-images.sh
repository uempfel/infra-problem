#!/usr/bin/env bash

# Usage: ./build-and-publish-images.sh <docker_repo> <tag> <tag-latest (default: false)>
set -e
set -u
set -o pipefail

docker_repo=$1
tag=$2
tag_latest=${3:-false}

docker login


declare -a filepaths=("static-assets" "newsfeed" "quotes" "front-end")
for path in "${filepaths[@]}"
do
    docker build -t ${docker_repo}/${path}:${tag} -f ${path}/Dockerfile .
    docker push ${docker_repo}/${path}:${tag}

    if [ $tag_latest = true ]
    then
        docker tag ${docker_repo}/${path}:${tag} ${docker_repo}/${path}:latest
        docker push ${docker_repo}/${path}:latest
    fi
done

echo "---SUMMARY---"
echo "Successfully built and published images using tag '${tag}'"

if [ $tag_latest = true ]
then
    echo "Tagged and published images with 'latest'"
fi

echo "To update the k8s deployment with the built images, run: "
echo "TAG=${tag} REPO=${docker_repo} envsubst < k8s.yaml.tpl | kubectl apply -f -"
