apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: front-end
  name: front-end
spec:
  replicas: 1
  selector:
    matchLabels:
      app: front-end
  template:
    metadata:
      labels:
        app: front-end
    spec:
      containers:
      - image: 12balu34/front-end:${TAG}
        name: front-end
        env:
        - name: APP_PORT
          value: "8080"
        - name: STATIC_URL
          value: "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6"
        - name: QUOTE_SERVICE_URL
          value: "http://quotes:8080"
        - name: NEWSFEED_SERVICE_URL
          value: "http://newsfeed:8080"
        - name: NEWSFEED_SERVICE_TOKEN
          valueFrom:
            secretKeyRef:
              name: token
              key: value
        livenessProbe:
          httpGet:
            path: /ping
            port: 8080
          initialDelaySeconds: 60
          periodSeconds: 5
---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: newsfeed
  name: newsfeed
spec:
  replicas: 1
  selector:
    matchLabels:
      app: newsfeed
  template:
    metadata:
      labels:
        app: newsfeed
    spec:
      containers:
      - image: 12balu34/newsfeed:${TAG}
        name: newsfeed
        env:
        - name: APP_PORT
          value: "8080"
        livenessProbe:
          httpGet:
            path: /ping
            port: 8080
          initialDelaySeconds: 60
          periodSeconds: 5
---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: quotes
  name: quotes
spec:
  replicas: 1
  selector:
    matchLabels:
      app: quotes
  template:
    metadata:
      labels:
        app: quotes
    spec:
      containers:
      - image: 12balu34/quotes:${TAG}
        name: quotes
        env:
        - name: APP_PORT
          value: "8080"
        livenessProbe:
          httpGet:
            path: /ping
            port: 8080
          initialDelaySeconds: 60
          periodSeconds: 5
---
apiVersion: v1
data:
  value: VDEmZVdiWVhOV0cxdzFeWUdLRFB4QVdKQF5ldF4ma1g=
kind: Secret
metadata:
  name: token
---
apiVersion: v1
kind: Service
metadata:
  labels:
    app: front-end
  name: front-end
spec:
  ports:
  - port: 80
    protocol: TCP
    targetPort: 8080
  selector:
    app: front-end
  type: LoadBalancer
---
apiVersion: v1
kind: Service
metadata:
  labels:
    app: quotes
  name: quotes
spec:
  ports:
  - port: 8080
    protocol: TCP
    targetPort: 8080
  selector:
    app: quotes
---
apiVersion: v1
kind: Service
metadata:
  labels:
    app: newsfeed
  name: newsfeed
spec:
  ports:
  - port: 8080
    protocol: TCP
    targetPort: 8080
  selector:
    app: newsfeed
---
