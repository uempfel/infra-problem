# DevOps Assessment

This project contains three services:

* `quotes` which serves a random quote from `quotes/resources/quotes.json`
* `newsfeed` which aggregates several RSS feeds together
* `front-end` which calls the two previous services and displays the results.

## Prerequisites

* Java
* [Leiningen](http://leiningen.org/) (can be installed using `brew install leiningen`)

## Running tests

You can run the tests of all apps by using `make test`

## Building

First you need to ensure that the common libraries are installed: run `make libs` to install them to your local `~/.m2` repository. This will allow you to build the JARs.

To build all the JARs and generate the static tarball, run the `make clean all` command from this directory. The JARs and tarball will appear in the `build/` directory.

### Static assets

`cd` to `front-end/public` and run `./serve.py` (you need Python3 installed). This will serve the assets on port 8000.

## Running

All the apps take environment variables to configure them and expose the URL `/ping` which will just return a 200 response that you can use with e.g. a load balancer to check if the app is running.

### Front-end app

`java -jar front-end.jar`

*Environment variables*:

* `APP_PORT`: The port on which to run the app
* `STATIC_URL`: The URL on which to find the static assets
* `QUOTE_SERVICE_URL`: The URL on which to find the quote service
* `NEWSFEED_SERVICE_URL`: The URL on which to find the newsfeed service
* `NEWSFEED_SERVICE_TOKEN`: The authentication token that allows the app to talk to the newsfeed service. This should be treated as an application secret. The value should be: `T1&eWbYXNWG1w1^YGKDPxAWJ@^et^&kX`

### Quote service

`java -jar quotes.jar`

*Environment variables*

* `APP_PORT`: The port on which to run the app

### Newsfeed service

`java -jar newsfeed.jar`

*Environment variables*

* `APP_PORT`: The port on which to run the app

## Proposed solution
* The proposed solution aims to provide a fast and easy way to provision a basic execution environment in the cloud. The main goal is to get the team working on the apps again as soon as possible.  
* To deploy the microservices, a basic Kubernetes cluster is provisioned at Digitalocean. The benefits and drawbacks of using Digitalocean are discussed in a bit more detail [below](#benefits-and-drawbacks-of-the-cloud-platform-and-services-used)
* Simple scripts to deploy the infrastructure via terraform, to build and publish the containers to Dockerhub and a k8s manifest are provided
* The application has been deployed with the code in this repo and the instructions provided below. It is accessible at http://infra-problem.dev.uempfel.de/


### Setup
At the current stage, the deployments are meant to be run on a dev's laptop.  
The following tools and credentials are needed (in addition to the ones to develop the app).   

#### Tools
| Name                                       | brew command             | Link docs                                                     | tested version |
|--------------------------------------------|--------------------------|---------------------------------------------------------------|----------------|
| `terraform`                                | `brew install terraform` | https://learn.hashicorp.com/tutorials/terraform/install-cli   | v1.0.9         |
| `doctl`                                    | `brew install doctl`     | https://docs.digitalocean.com/reference/doctl/how-to/install/ | 1.65.0         |
| `kubectl`                                  | `brew install kubectl`   | https://kubernetes.io/docs/tasks/tools/#kubectl               | 1.22.2         |
| `docker`                                   | n.A.                     | https://docs.docker.com/get-docker/                           |                |
| For MacOs: `gettext` (includes `envsubst`) | `brew install gettext`   | https://formulae.brew.sh/formula/gettext                      | 0.21           |

#### Accounts/ Credentials
| Description                                                      | Link to docs                                                                                       |
|------------------------------------------------------------------|----------------------------------------------------------------------------------------------------|
| Digitalocean API Token with `write` scope                        | https://docs.digitalocean.com/reference/api/create-personal-access-token/                          |
| Dockerhub repository and password                                | https://hub.docker.com                                                                             |
| GitLab Token (`api` scope, maintainer access rights to the repo) | https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token |

### Deploying the infrastructure
* The microservices will be deployed to a small, basic Kubernetes cluster at [Digitalocean](https://www.digitalocean.com/products/kubernetes/).  
* In order to be able to access the frontend via a url in addition to an IPv4 address, a domain and an A Record will be created.
* The infrastructure is managed by terraform. To prevent the state from being lost on one of the devs' machines, the state is stored in [GitLab's http backend](https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html).
* The terraform code is organized in a way that enables provisioning additional environments that follow the same blueprint in the future.

To provision the infrastructure, execute the following steps:  
1) Open a terminal window and `cd` to the root of this repo
2) Run the script to create the infrastructure
  ```bash
  cd infra
  # Usage: ./create-infra.sh <stage> <gitlab username> <gitlab token>
  # Note that the stage needs to correspond to the name of the folder for the tf config files
  ./create-infra.sh dev my.username some-token
  # You will be promted for your Digitalocean Token
  # Terraform will ask you if you want to apply the changes
  ```
3) Get the kubeconfig for the newly provisioned cluster
  ```bash
  # doctl kubernetes cluster kubeconfig save <stage>
  doctl kubernetes cluster kubeconfig save dev
  # check if you can access the cluster
  kubectl cluster-info
  ```

### Building and deploying the microservices
* In order to deploy the microservices to the k8s cluster, they need to be containerised and published to an image registry

To build, publish and deploy the microservices, execute the following steps:  
1) Build the jar files as desribed in the [Building section](#building) section above
2) Open a terminal window and `cd` to the root of this repo
3) Run the `build-and-publish-images.sh` script
  ```bash
  # Usage: ./build-and-publish-images.sh <docker_repo> <tag> <tag-latest (default: false)>
  # Set <tag-latest> to true, to automatically tag the images with 'latest'
  # The tag could be a version that you continuously increment or as you add changes or
  # the repo's current commit hash (git rev-parse --short HEAD)
  ./build-and-publish-images.sh 12balu34 v0.0.3 true
  ```
4) Deploy the newly created images to k8s by rendering and applying the `k8s.yaml.tpl` manifest
  ```bash
  # Set TAG to the value you used in the previous step and REPO to your dockerhub repo
  TAG=v0.0.3 REPO=12balu34 envsubst < k8s.yaml.tpl | kubectl apply -f -
  ```

5) After the deployment, run the following command to retrieve the IP where the frontend will be available (note that this might take a while):
  ```bash
  kubectl get svc front-end --output jsonpath='{.status.loadBalancer.ingress[0].ip}
  ```

6) After initial deployment only: to access the front-end via a url in addition to the IP,  
   edit the variable `ipv4_address` in the stages' `terraform.tfvars` file and
   execute the `create-infra.sh` script again.

### Future work
Given the limited amount of time, some tradeoffs were made that should be addressed in the future. The following list contains some of the issues and brief recommendations.

> Container images are published publicly at dockerhub.  
  * They should be moved to a private image repository  

> All microservices are currently tagged the same way. This was done to keep the build and publishing step as simple as possible for the current MVP.  
  * I would recommend to manage the releases of the individual services seperately

> It was assumed that GitLab can be used.  
  * If that doesn't work for the team, the terraform state should be transferred to a different backend (e.g. S3)

> The scripts provided need human input and are executed on the devs' laptops, which are configured differently.  
  * All steps related to builiding, testing and deploying the application should be executed via a CI/CD pipeline to increase consistency, transparency and auditability   

> The terraform code has been structured in a way to allow more environments to be provisioned with the same set of files. The script however was designed to be used by humans, not machines. 
  * To implement an infrastructure pipeline, I recommend following the concepts outlined in this article: https://www.thoughtworks.com/insights/blog/infrastructure-pipelines
  * A more hands-on article using GitLab CI is available here: https://docs.gitlab.com/ee/user/infrastructure/iac/  

> As of now, no RBAC or network policies were implemented in the kubernetes cluster. For a production scenario, I would recommend to work on these issues.
  * We should limit the "kubectl access" to the production cluster drastically. Ideally, only the CI server is allowed to interact with the Kubernetes API
  * We should implement network policies within Kubernetes to control the access to the microservices from within the cluster

> Currently, neither the infrastructure nor the k8s deployment is configured for high availability. The manifest provided is fairly static.
  * I would recommend creating a Helm-chart to make the deployment more configurable. As an example, this would allow to specifiy the replica count of the services
  * To increase infrastructure availability, the number cluster nodes should be increased in a production environment

> Moving to production, a monitoring and alerting solution must be installed and configured. 
  * For Kubernetes, I would recommend Prometheus and Grafana


#### How I would engage with the team
It is important to continously get feedback from the team. I would offer support for any questions regarding the infrastructure or the build process. The following questions could start an ongoing conversation: 
* What's your level of expertise with containers and Kubernetes?
* Is it ok to publish the images publicly?
* How is the workflow working for you? 
  * What's the most time consuming task related to the deployment?
  * What should be improved?
* How would you prioritize the issues outlined above?

### Benefits and drawbacks of the cloud platform and services used
Digitalocean was chosen for two primary reasons:
1) Ease of use compared to bigger cloud providers like AWS, GCP or Azure
2) My familiarity with provisioning a basic Kubernetes environment at Digitalocean to gain speed

* While Digitalocean's service offering is not as big as that of the big three (AWS, GCP, Azure), the process of deploying a simple k8s cluster is comparably easy. Only a few resources need to be managed. The cost of running a managed cluster is also quite low, the estimated cost of the current setup is around 30 USD per month.  
* One major downside of using Digitalocean is the small number of regions available compared to the big three. Digitalocean doesn't have the fine-granular IAM policies available at other cloud providers. Also, if more service need to be consumed in the future, the offering at Digitalocean is fairly limited.  
* With Kubernetes as the deployment target however, it is possible to move to a bigger cloud provider if the requirements cannot be fulfilled anymore. The process of building and deploying the applications would (mostly) stay stable.
